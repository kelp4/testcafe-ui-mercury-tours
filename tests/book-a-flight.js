import { Selector } from 'testcafe'

fixture`Mercury Tours E2E`.page`http://newtours.demoaut.com/`.httpAuth({
  username: 'mercury',
  password: 'mercury'
})

test('book-a-flight', async t => {
  await t
    //Login
    .expect(
      Selector('td')
        .withText('User Name:')
        .nth(5)
        .find('font').innerText
    )
    .ok()
    .typeText(Selector('[name="home"]').find('[name="userName"]'), 'mercury')
    .pressKey('tab')
    .typeText(Selector('[name="home"]').find('[name="password"]'), 'mercury')
    .click(Selector('[name="home"]').find('[name="login"]'))
    //Select Dates
    .expect(
      Selector('td')
        .withText('Use our Flight Finder to search for the lowest far')
        .nth(3)
        .find('font').innerText
    )
    .ok()
    .click(Selector('[name="passCount"]'))
    .click(
      Selector('[name="passCount"]')
        .find('option')
        .withText('3')
    )
    .click(Selector('[name="fromPort"]'))
    .click(
      Selector('[name="fromPort"]')
        .find('option')
        .withText('Frankfurt')
    )
    .click(Selector('[name="fromMonth"]'))
    .click(
      Selector('[name="fromMonth"]')
        .find('option')
        .withText('April')
    )
    .click(Selector('[name="toPort"]'))
    .click(
      Selector('[name="toPort"]')
        .find('option')
        .withText('San Francisco')
    )
    .click(Selector('[name="toMonth"]'))
    .click(
      Selector('[name="toMonth"]')
        .find('option')
        .withText('June')
    )
    .click(
      Selector('[name="findflight"]')
        .find('[name="servClass"]')
        .nth(2)
    )
    .click(Selector('[name="airline"]'))
    .click(Selector('option').withText('Unified Airlines'))
    .click(Selector('[name="findflight"]').find('[name="findFlights"]'))
    //Select Flights
    .expect(
      Selector('b')
        .withText('DEPART')
        .find('font').innerText
    )
    .ok()
    .expect(
      Selector('b')
        .withText('Frankfurt to San Francisco')
        .find('font').innerText
    )
    .ok()
    .expect(
      Selector('b')
        .withText('RETURN')
        .find('font').innerText
    )
    .ok()
    .expect(
      Selector('b')
        .withText('San Francisco to Frankfurt')
        .find('font').innerText
    )
    .ok()
    .click(Selector('b').withText('San Francisco to Frankfurt'))
    .click(Selector('[name="results"]').find('[name="reserveFlights"]'))
    //Purchase Flights
    .click(Selector('[name="bookflight"]').find('[name="passFirst0"]'))
    .typeText(
      Selector('[name="bookflight"]').find('[name="passFirst0"]'),
      'john'
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="passLast0"]'),
      'smith'
    )
    .click(Selector('[name="pass.0.meal"]'))
    .click(
      Selector('[name="pass.0.meal"]')
        .find('option')
        .withText('Diabetic')
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="passFirst1"]'),
      'abby'
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="passLast1"]'),
      'smith'
    )
    .click(Selector('[name="pass.1.meal"]'))
    .click(
      Selector('[name="pass.1.meal"]')
        .find('option')
        .withText('Low Sodium')
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="passFirst2"]'),
      'jane'
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="passLast2"]'),
      'smith'
    )
    .click(Selector('[name="pass.2.meal"]'))
    .click(
      Selector('[name="pass.2.meal"]')
        .find('option')
        .withText('Low Cholesterol')
    )
    .click(Selector('[name="creditCard"]'))
    .typeText(
      Selector('[name="bookflight"]').find('[name="creditnumber"]'),
      'e'
    )
    .typeText(
      Selector('[name="bookflight"]').find('[name="creditnumber"]'),
      '3243245324324'
    )
    .click(Selector('[name="cc_exp_dt_mn"]'))
    .click(
      Selector('[name="cc_exp_dt_mn"]')
        .find('option')
        .withText('04')
    )
    .click(Selector('[name="cc_exp_dt_yr"]'))
    .click(Selector('option').withText('2001'))
    .click(Selector('[name="bookflight"]').find('[name="buyFlights"]'))
    .expect(
      Selector('b')
        .withText('Your itinerary has been booked!')
        .find('font')
        .nth(1).innerText
    )
    .ok()
    .click(
      Selector('td')
        .withText('Your itinerary has been booked!')
        .nth(2)
        .find('table')
        .find('tbody')
        .find('tr')
        .nth(21)
        .find('td')
        .find('table')
        .find('tbody')
        .find('tr')
        .find('td')
        .nth(2)
        .find('a')
        .find('img')
    )
})
